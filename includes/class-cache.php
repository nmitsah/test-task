<?php

namespace App;

class Cache {
	public function __construct()
	{
		$this->host = '127.0.0.1';
		$this->port = '11211';
		$this->memcache = new \Memcache();
		$this->is_connected = false;
	}

	public function connect()
	{
		return !$this->is_connected ? $this->memcache->connect($this->host, $this->port) : true;
	}

	public function disconnect()
	{
		return $this->is_connected ? $this->memcache->close() : true;
	}

	public function set($key, $data, $time = 3600)
	{
		if ($this->connect()) {
			$this->memcache->set($key, $data, 0, $time);
		}
	}

	public function get($key)
	{
		$result = [];
		if ($this->connect()) {
			$result = $this->memcache->get($key);
		}
		return $result;
	}
}
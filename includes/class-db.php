<?php

namespace App;

class DB {
	public function __construct(Cache $cache)
	{
		$host = '127.0.0.1';
		$db = 'php_test_task';
		$user = 'mysql';
		$password = 'mysql';
		$charset = 'utf8';

		$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
		$options = [
			\PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
			\PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
			\PDO::ATTR_EMULATE_PREPARES   => false,
		];
		$this->pdo = new \PDO($dsn, $user, $password, $options);
		$this->fields = ['id', 'name', 'symbol', 'website_slug', 'rank', 'circulating_supply', 'total_supply', 'max_supply', 'quotes', 'last_updated'];
		$this->cache = $cache;
	}

	public function save_currencies($data)
	{
		$result = [
			'success' => false,
			'errors'  => []
		];

		if ($data) {
			$fields = $this->fields;

			foreach ($data as $item) {
				$keys = array_keys($item);

				if (($keys) !== $fields) continue;

				$keys = implode(', ', $keys);

				$query = $this->pdo->prepare("
					INSERT INTO currencies ($keys)
					VALUES (:id, :name, :symbol, :website_slug, :rank, :circulating_supply, :total_supply, :max_supply, :quotes, :last_updated)");

				$query_result = $query->execute([
					'id' => $item['id'],
					'name' => $item['name'],
					'symbol' => $item['symbol'],
					'website_slug' => $item['website_slug'],
					'rank' => $item['rank'],
					'circulating_supply' => $item['circulating_supply'],
					'total_supply' => $item['total_supply'],
					'max_supply' => $item['max_supply'],
					'quotes' => json_encode($item['quotes']),
					'last_updated' => date('Y-m-d H:i:s', $item['last_updated'])
				]);

				if (!$query_result) {
					$result['errors'][$item['id']] = 'Currency ' . $item['id'] . ' not inserted.';
				}
			}

			if (!$result['errors']) {
				$result['success'] = true;
			}
		}

		return $result;
	}

	public function get_currencies()
	{
		$key = 'currencies';

		if (!$data = $this->cache->get($key)) {
			$data = $this->pdo->query('SELECT * FROM currencies')->fetchAll(\PDO::FETCH_UNIQUE);
			$data = json_encode($data);
			$this->cache->set($key, $data);
		}

		return $data;
	}
}
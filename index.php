<?php

require_once('loader.php');

if (isset($_GET['parse_data'])) {
	include_once 'parser.php';
	return;
}

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Title</title>
        <script src="https://code.highcharts.com/stock/highstock.js"></script>
		<link rel="stylesheet" href="<?= PROJECT_ASSETS_URI . '/css/style.css' ?>">
		<script src="<?= PROJECT_ASSETS_URI . '/js/script.js' ?>"></script>
	</head>
	<body>
		<div id="container">
			<?php
				include_once(PROJECT_TEMPLATES_PATH . '/trade_chart.html');
				include_once(PROJECT_TEMPLATES_PATH . '/buysell_chart.html');
			?>
			<script id="serialized-data"><?php echo $db->get_currencies() ?></script>
		</div>
	</body>
</html>

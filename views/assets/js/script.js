var data;

document.addEventListener('DOMContentLoaded', function(){
    data = document.getElementById('serialized-data');


    var renderTradeChart = function(data) {
        var mainCurrency = data[0],
            mainCurrencyTrading = mainCurrency.quotes.USD,
            lastUpdateTime = (new Date(mainCurrency.last_updated)),
            chartData = [];

        delete data[0];
        chartData.push(
            [lastUpdateTime.setHours(lastUpdateTime.getHours() - 1), mainCurrencyTrading.price + (mainCurrencyTrading.price * (mainCurrencyTrading.percent_change_1h / 100))],
            [lastUpdateTime.setHours(lastUpdateTime.getHours() - 23), mainCurrencyTrading.price + (mainCurrencyTrading.price * (mainCurrencyTrading.percent_change_24h / 100))],
            [lastUpdateTime.setHours(lastUpdateTime.getHours() - 144), mainCurrencyTrading.price + (mainCurrencyTrading.price * (mainCurrencyTrading.percent_change_7d / 100))]
        );

        Highcharts.stockChart('trade-chart', {
            rangeSelector: {
                enabled: false
            },
            navigator: {
                enabled: false
            },
            title: {
                text: mainCurrency.name + ' price chart'
            },
            series: [{
                name: mainCurrency.symbol,
                data: chartData,
                tooltip: {
                    valueDecimals: 4
                }
            }]
        });
    };

    var renderTradeTable = function (data) {
        var container = document.getElementById('buy-sell-chart');
        debugger;
        Object.values(data).forEach(function(elem){
            var currencyRow, nameEl, marktCapEl, priceEl, circlSupplyEl, volumeEl, pHourEl, pDayEl, pWeekEl, quotes = elem.quotes.USD;
            currencyRow = document.createElement('div');
            currencyRow.classList.add('row');
            nameEl = document.createElement('span');
            nameEl.classList.add('name');
            nameEl.innerText = elem.name;
            marktCapEl = document.createElement('span');
            marktCapEl.classList.add('market-cap');
            marktCapEl.innerText = quotes.market_cap;
            priceEl = document.createElement('span');
            priceEl.classList.add('price');
            priceEl.innerText = quotes.price;
            circlSupplyEl = document.createElement('span');
            circlSupplyEl.classList.add('cir-supp');
            circlSupplyEl.innerText = elem.circulating_supply;
            volumeEl = document.createElement('span');
            volumeEl.classList.add('vol');
            volumeEl.innerText = quotes.volume_24h;
            pHourEl = document.createElement('span');
            pHourEl.classList.add('p-hour');
            pHourEl.innerText = quotes.percent_change_1h;
            pDayEl = document.createElement('span');
            pDayEl.classList.add('p-day');
            pDayEl.innerText = quotes.percent_change_24h;
            pWeekEl = document.createElement('span');
            pWeekEl.classList.add('p-week');
            pWeekEl.innerText = quotes.percent_change_7d;
            currencyRow.appendChild(nameEl);
            currencyRow.appendChild(marktCapEl);
            currencyRow.appendChild(priceEl);
            currencyRow.appendChild(circlSupplyEl);
            currencyRow.appendChild(volumeEl);
            currencyRow.appendChild(pHourEl);
            currencyRow.appendChild(pDayEl);
            currencyRow.appendChild(pWeekEl);
            container.appendChild(currencyRow);
        });
    };

    if (data && data.innerText) {
        data = Object.values(JSON.parse(data.innerText));

        data.forEach((el) => {
            el.quotes = JSON.parse(el.quotes);
        });

        renderTradeChart(data);
        renderTradeTable(data);
    }
});

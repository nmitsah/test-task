<?php

$ds = DIRECTORY_SEPARATOR;
define('PROJECT_ROOT_PATH', __DIR__);
define('PROJECT_ASSETS_PATH', PROJECT_ROOT_PATH . $ds . 'views' . $ds . 'assets');
define('PROJECT_TEMPLATES_PATH', PROJECT_ROOT_PATH . $ds . 'views' . $ds . 'templates');
define('PROJECT_ASSETS_URI', '/views/assets');

$file_pattern = $_SERVER['DOCUMENT_ROOT'] . $ds . 'includes' . $ds . '*.php';

$classes = ['Cache', 'DB'];

foreach ($classes as $class) {
	$filename = 'class-' . strtolower($class);
	if (file_exists($file = str_replace('*', $filename, $file_pattern))) {
		require_once $file;
	}
}

if (!class_exists('App\Cache') || !class_exists('App\DB')) {
	die('Some of necessary files are missing!');
}

$cache = new App\Cache();
$db    = new App\DB($cache);